# Copyright (c) 2018-2019 Univ. Bordeaux


all: clean run

clean:
	rm -rf *.sage.py

run: #setup scheduling_algos
	sage simulator.sage