# Copyright (c) 2018-2019 Univ. Bordeaux






# ********************************** BEGINNING SIMULATOR ********************************** #
## Beginning basic imports ##
import csv
from math import *
from random import *
from copy import deepcopy
from itertools import *
from csv import *
seed()
## End basic imports ##







# -------------------------- Setup -------------------------- #



### Please enter your path where you would like to write your csv and txt files ###
path_to_file = "output/"

### System Features ###
nodes = 50 # Number of nodes
cores = 8 # Number of cores per node
memory = 100000 # Total memory of the system
alphaAmdhal = 0 # In current version of the simulator, must be set to 0
simulation = 1000 # Time to run the simulation on 1 core
memory_simu = 75000 # Amount of memory consumed by simulation
number_analysis = 8 # Number of analysis
memory_per_node = memory / nodes # Memory equally shared between every node


### Represents the different bandwidth per node we want to try
### The bandwidth per node will be equal to factor*memory_per_node
bandwidth_factors = [0.1,0.25,0.5,1]


### Basically, simulation consumme 1 amount of memeory over 1.3333333 memory available
### This vector is used to generate set of analysis whose total memory peaks additionned to the simulation memory peak sums to given vector value
### When factor < 1.33, possibly all analysis can be done in-situ
total_memory_sim_ana_vector = [1.05,1.1,1.15,1.2,1.25,1.3,1.35,1.4,1.45,1.5,1.55,1.6]


### Number of time we test a configuration. Final result will be average of the number_tests runs.
### Useful to limite the influence of random generatio of analysis
number_tests = 130


### Number of time we specificly run random scheduling policy algorithm. Final result will be average of the random_algo_runs runs.
random_algo_runs = 50





def init():
	global scheduling_mode 
	# 1 = asynchronous, 2 = synchronous
	scheduling_mode = 1
















































# --------------------------  System class that models the plateform -------------------------- #


init()


# scheduling_mode must be either one (synchronous) or two (synchronous case)
assert (scheduling_mode>0 and scheduling_mode<3)


if (scheduling_mode==1):
	path_to_file += "Fichiers_asynchronous/"
elif(scheduling_mode==2):
	path_to_file += "Fichiers_synchronous/"




class System:

	def __init__(self, nodes, cores, memory, bandwidthPerNode, alphaAmdhal, Simulation, memory_Simu, Analysis,AnalysisIS,AnalysisIT):
		self.nodes = nodes # number of nodes of the system
		self.cores = cores # number of cores of the system
		self.nodesIS = (memory_Simu)/(memory / self.nodes)
		self.coresIS = 1
		self.memory = memory # total memory of the system
		self.memoryPerNode = memory / self.nodes # memory available per node
		self.bandwidthPerNode = bandwidthPerNode # bandwidth per node
		self.alphaAmdhal = alphaAmdhal # alpha of Amdhal's law (1 = fully parallel)
		self.Simulation = Simulation # simulation function
		self.memory_Simu = memory_Simu # percentage of the total memory used by the simulation
		self.Analysis = list(Analysis) # set of Analysis functions: list of analysis where each analysis is a list containing the  time to process analysis,the memory peak of the analysis, a counter representing the number of time we have to do this analysis
		self.S = 0
		for analysis in self.Analysis:
			for _ in range(0,analysis[2]):
				self.S+=analysis[0] 
		self.AnalysisIS = list(AnalysisIS) # set of Analysis functions to be done in-situ
		self.X = 0
		for analysis in self.AnalysisIS:
			for _ in range(0,analysis[2]):
				self.X+=analysis[0]
		self.AnalysisIT = list(AnalysisIT) # set of Analysis functions to be processed in-transit
		self.resource_partitioning()
		self.current_analysis_mode = ""
		self.possible_analysis_mode = set()
		self.execution_time = infinity 
		self.execution_time = scheduling(self)
		self.possible_analysis_mode.add(self.current_analysis_mode)







	def to_string(self):
		print("\n\n### SYSTEM ###")
		print("  # of nodes = "+str(self.nodes))
		print("  # of cores = "+str(self.cores))
		print("  # of IS nodes = "+str(self.nodesIS))
		print("  # of IS cores = "+str(self.coresIS))
		print("  total memory = "+str(self.memory))
		print("  memory per node = "+str(self.memoryPerNode))
		print("  Memory used for Simulation = "+str(self.memory_Simu))
		print("  bandwidth per node = "+str(self.bandwidthPerNode))
		print("  alpha for Amdhal's law = "+str(self.alphaAmdhal))
		print("  Simulation (time to run it on 1 core) = "+str(self.Simulation))
		print("  Analysis functions [time,peak,occurence] = "+str(self.Analysis))
		print("  Total analysis load: S = "+str(self.S))
		print("  Analysis IS = "+str(self.AnalysisIS))
		print("  IS analysis load = "+str(self.X))
		print("  Analysis IT = "+str(self.AnalysisIT))
		print("  Execution time = "+str(self.execution_time))
		print("  Possible analysis mode = "+str(self.possible_analysis_mode)+"\n\n")







	def as_string(self):
		r = ""
		r += "\n### SYSTEM ###\n"
		r += "  # of nodes = "+str(self.nodes)+"\n"
		r += "  # of cores = "+str(self.cores)+"\n"
		r += "  # of IS nodes = "+str(self.nodesIS)+"\n"
		r += "  # of IS cores = "+str(self.coresIS)+"\n"
		r += "  total memory = "+str(self.memory)+"\n"
		r += "  memory per node = "+str(self.memoryPerNode)+"\n"
		r += "  Memory used for Simulation = "+str(self.memory_Simu)+"\n"
		r += "  bandwidth per node = "+str(self.bandwidthPerNode)+"\n"
		r += "  alpha for Amdhal's law = "+str(self.alphaAmdhal)+"\n"
		r += "  Simulation (time t o run it on 1 core) = "+str(self.Simulation)+"\n"
		r += "  Analysis functions [time,peak,occurence] = "+str(self.Analysis)+"\n"
		r += "  Total analysis load: S = "+str(self.S)+"\n"
		r += "  Analysis IS = "+str(self.AnalysisIS)+"\n"
		r += "  IS analysis load = "+str(self.X)+"\n"
		r += "  Analysis IT = "+str(self.AnalysisIT)+"\n"
		r += "  Possible analysis mode = "+str(self.possible_analysis_mode)+"\n"
		r += "  Execution time = "+str(self.execution_time)+"\n\n\n"
		return r







	# Given a current state of System (Nodes, Cores, alphaAmdhal + AnaIS, AnaIT), compute an (n*,c*) associate
	def resource_partitioning(self):
		# min number of in situ nodes for the simulation to fit in memory
		min_IS_nodes = ceil((self.memory_Simu)/(self.memoryPerNode))
		
		memIS = 0
		for analysis in self.AnalysisIS:
			for _ in range(0,analysis[2]):
				memIS += analysis[1]

		loadIT = 0
		workIT = 0
		for ana in self.AnalysisIT:
			for _ in range(0,ana[2]):
				loadIT += ana[1]
				workIT += ana[0]


		# full in situ setup, just need to compute the optimal number of c*, n* is the system number of nodes
		if (not self.AnalysisIT and self.AnalysisIS):
			self.nodesIS = self.nodes
			self.coresIS = (self.X*self.cores)/(self.Simulation+self.X)
			if (ceil(self.coresIS)==self.cores):
				self.coresIS = floor(self.coresIS)
			else:
				self.coresIS = ceil(self.coresIS) # self.coresIS

		# full in transit setup, no IS cores. Need to compute the good number of in situ nodes
		elif(not self.AnalysisIS and self.AnalysisIT):
			self.coresIS = 0
			self.nodesIS = (((self.Simulation*self.nodes)/(self.cores-self.coresIS)) / ((self.cores+self.coresIS)*((workIT/self.cores)+(loadIT/self.bandwidthPerNode))+(self.Simulation/(self.cores-self.coresIS))))
			# Ensure there is at least a node for IT
			if (ceil(self.nodesIS)>=self.nodes):
				self.nodesIS = self.nodes-1
			else:
				self.nodesIS = ceil(self.nodesIS)

		# mixture of in transit / in situ analysis. Compute associated n* and c* value
		else:
			self.coresIS = (self.X*self.cores)/(self.Simulation+self.X)
			#self.nodesIS = ((self.memory_Simu + memIS)/(self.memoryPerNode))
			assert(self.coresIS!=0)
			self.nodesIS = (self.X*self.nodes) / ((self.coresIS*((workIT/self.cores)+(loadIT/self.bandwidthPerNode)))+self.X)

			
			if (ceil(self.coresIS)==self.cores):
				self.coresIS = floor(self.coresIS)
			else:
				self.coresIS = ceil(self.coresIS) # self.coresIS
			
			# Ensure there is at least a node for IT
			if (ceil(self.nodesIS)>=self.nodes):
				self.nodesIS = self.nodes-1
			else:
				self.nodesIS = ceil(self.nodesIS)

		if (self.nodesIS<min_IS_nodes):
			self.nodesIS = min_IS_nodes
			
		assert(self.nodesIS != 0)
		assert(self.nodesIS >= min_IS_nodes)







	def system_update_IT_to_IS(self, index):
		present_in_AnalysisIS = False
		i = 0
		while ((not present_in_AnalysisIS) and (i<len(self.AnalysisIS))):
			if ( (compare_peak(self.AnalysisIS[i],self.Analysis[index])==0 and compare_exec_time(self.AnalysisIS[i],self.Analysis[index])==0)):
				present_in_AnalysisIS = True
			else:
				i+=1

		if (present_in_AnalysisIS):
			self.AnalysisIS[i][2]+=1

		else:
			ana = list(self.Analysis[index])
			ana[2]=1
			self.AnalysisIS.append(ana)
			
		self.AnalysisIT = self.complementary_set(list(self.AnalysisIS))

		
		self.X = 0
		for analysis in self.AnalysisIS:
			for _ in range(0,analysis[2]):
				self.X+=analysis[0]
		self.resource_partitioning()
		assert(self.coresIS <= self.cores)
		assert(self.nodesIS <= self.nodes)
		assert(self.X <= self.S)







	def complementary_set(self,subset_analysis):
		res = []
		if (len(self.Analysis)==1):
			assert(len(subset_analysis)==1)
			ana = subset_analysis[0]
			ana_copy = list(ana)
			ana_copy[2]=0
			if(self.Analysis[0][2]-ana[2]):
				res.append(list(ana_copy))
				for _ in range(0,self.Analysis[0][2]-ana[2]):
					res[0][2]+=1        
		else:
			for analysis in self.Analysis:
				present_in_subset_analysis = False
				index = 0
				while ((not present_in_subset_analysis) and (index<len(subset_analysis))):
					if ( (compare_peak(analysis,subset_analysis[index])==0 and compare_exec_time(analysis,subset_analysis[index])==0)):
						present_in_subset_analysis = True
						instances_to_add = analysis[2]-subset_analysis[index][2]
						if (instances_to_add):
							ana_to_add = list(analysis)
							ana_to_add[2] = instances_to_add
							res.append(ana_to_add)
					index+=1

				if (not present_in_subset_analysis):
					res.append(list(analysis))

		return res







	def set_IS_analysis(self,list_IS_analysis):
		self.AnalysisIS = list(list_IS_analysis)
		self.AnalysisIT = list(self.complementary_set(list_IS_analysis))

		# print(self.AnalysisIS)
		# print(self.AnalysisIT)
		# print(self.Analysis)

		self.X = 0
		for analysis in self.AnalysisIS:
			for _ in range(0,analysis[2]):
				self.X+=analysis[0]
		self.resource_partitioning()
		# assert(self.coresIS <= self.cores)
		# assert(self.nodesIS <= self.nodes)
		# assert(self.X <= self.S)    







	def set_IT_analysis(self,list_IT_analysis):
		self.AnalysisIT = list(list_IT_analysis)
		list_IS_analysis = self.complementary_set(list_IT_analysis)
		self.AnalysisIS = list(list_IS_analysis)
		self.X = 0
		for analysis in self.AnalysisIS:
			for _ in range(0,analysis[2]):
				self.X+=analysis[0]
		self.resource_partitioning()
		assert(self.coresIS <= self.cores)
		assert(self.nodesIS <= self.nodes)
		assert(self.X <= self.S)      







































# --------------------------  Annexe functions -------------------------- #



# Quick-sort implementation
# Parameters: array=list of element to be sorted, compare=comparison function for two elements of array (cmp by default) 
def quicksort(array,compare=cmp):
	less = []
	equal = []
	greater = []

	if len(array)>1:
		random_index = randrange(0,len(array))
		pivot = array[random_index]
		for x in array:
			if compare(x,pivot)==-1 :
				less.append(x)
			if compare(x,pivot)==0:
				equal.append(x)
			if compare(x,pivot)==1:
				greater.append(x)
		return quicksort(less,compare)+equal+quicksort(greater,compare) 
	else: #stopping the recursion
		return array







## We use a simple model for communication: time = volumeToSend/bandwidth
def communication(volumeToSend,numberSendingNodes,bandwidthPerNode):
	if (numberSendingNodes==0):
		return infinity
	else:
		return (volumeToSend / (numberSendingNodes * bandwidthPerNode)) 







### Function that checks the respect of memory constraint s
### The function checks whereas the memory for IS analysis and simulation is sufficient on the nodesIS of the system system
def checking_viability(system):
	#if nodesIS is greater than the number of nodes, it means that the required IS memory is too big
	if (system.nodesIS > system.nodes):
		return False

	IS_memory = (system.memoryPerNode * system.nodesIS) - system.memory_Simu

	if(IS_memory < 0):
		return False

	m = 0
	for analysis in system.AnalysisIS:
		for _ in range(0,analysis[2]):
			m += deepcopy(analysis[1])
	return (m<=IS_memory)







### Function that compares two analysis with regards to their memory peak
def compare_peak(analysis1,analysis2):

	if (analysis1==[] and analysis2==[]):
		return 0

	if ( (analysis1==[] and analysis2!=[]) or (analysis1!=[] and analysis2==[]) ):
		return -1

	if analysis1[1]>analysis2[1]:
		return 1
	if analysis1[1]==analysis2[1]:
		return 0
	if analysis1[1]<analysis2[1]:
		return -1   







### Function that compares two analysis with regards to their execution time
def compare_exec_time(analysis1,analysis2):
	if (analysis1==[] and analysis2==[]):
		return 0

	if ( (analysis1==[] and analysis2!=[]) or (analysis1!=[] and analysis2==[]) ):
		return -1
	
	if analysis1[0]>analysis2[0]:
		return 1
	if analysis1[0]==analysis2[0]:
		return 0
	if analysis1[0]<analysis2[0]:
		return -1   







### Function that generates a set of simulation function of size number_analysis (cf setup.sage)
### The exec time of each function is randomly selected between a lower bound and upper bound percentage of simulation
### The memory peaks of the analysis are randomly generated but their sum + the simulation memory sums to parameter total_memory_sim_ana
def generate_analysis_functions_wrt_simulation(simulation, memory_simu, total_memory_sim_ana,number_analysis):
	nu_analysis = int(number_analysis)
	available_mem_analysis = total_memory_sim_ana - memory_simu
	memory_vector_analysis = []
	cpt_mem = 0.0
	for _ in range(0,nu_analysis):
		tmp = random()
		cpt_mem+=tmp
		memory_vector_analysis.append(tmp)
	
	cpt = 0.0
	analysis = []
	for mem in memory_vector_analysis:
		l = []
		m = (mem/cpt_mem)*available_mem_analysis
		cpt+=m
		l.append(randint(int(simulation*0.05),int(simulation*0.25)))
		l.append(m)
		test=False
		i = 0
		while (i<len(analysis) and not test):
			if ((compare_peak(l,analysis[i])==0 and compare_exec_time(l,analysis[i])==0)):
				test = True
			else:
				i+=1
		if (test):
			analysis[i][2]+=1
		else:
			l.append(1)
			analysis.append(list(l))
	return analysis







### Intermediate function to generate subsets of analysis
def generate_subsets_index(liste):
	p = []
	i, imax = 0, 2**len(liste)-1
	while i <= imax:
		s = []
		j, jmax = 0, len(liste)-1
		while j <= jmax:
			if (i>>j)&1 == 1:
				s.append(liste[j])
			j += 1
		p.append(s)
		i += 1 
	return p







### Generate all subsets of the liste given in parameters
def generate_subsets(liste):
	res = []
	test = True
	for ana in liste:
		if(ana[2]>1):
			test = False

	if (test):
		index_liste =  range(0,len(liste),1)

		tmp = generate_subsets_index(index_liste)

		for l in tmp:
			r = []
			for index in l:
				r.append(liste[index])
			res.append(r)
		return res 

	else:
		counter = 0
		index_liste = []
		for ana in liste:
			for _ in range(0,ana[2]):
				index_liste.append(counter)
			counter+=1
		tmp = generate_subsets_index(index_liste)


		for l in tmp:
			r = []
			for index in l:
				analysis = list(liste[index])
				analysis[2] = 1
				test = False
				i = 0
				while (i<len(r) and not test):
					if ((compare_peak(analysis,r[i])==0 and compare_exec_time(analysis,r[i])==0)):
						test = True
					else:
						i+=1
				if (test):
					r[i][2]+=1
				else:
					r.append(analysis)

			res.append(r)
		return res







### Remove all the lists of length size in the set of lists liste
def remove_list_of_length(liste,size):
	res = list(liste)
	for element in res:
		if (len(element)==size):
			res.remove(element)
	return res







### Test the equality between liste1 and liste2 (equality means they contain same elements, whatever their order in the lists)
def is_equal(liste1,liste2):
	l = []
	for e in liste1:
		if (not e in liste2):
			return False
		else:
			l.append(e)

	return (len(l)==len(liste2))









































# --------------------------  Scheduling policies for Analysis functions -------------------------- #



### Return a system with a set of analysis in situ and a set of in-transit analysis
### Consider analysis by increasing memory peak
### Parameters: system initialized with all analysis in IT and none in IS
def maxIS_increasing_memory_peak(system,compare_peak,compare_exec_time):
	
	sys = deepcopy(system)

	sys.Analysis = list(quicksort(system.Analysis,compare_peak))
	
	exec_time = 0
		
	for index in range (0,len(sys.Analysis)):
		for _ in range(0,sys.Analysis[index][2]):
			#print("testing "+str(sys.Analysis[index]))
			#Save current state
			inSituAnalysis = deepcopy(sys.AnalysisIS)
			inTransitAnalysis = deepcopy(sys.AnalysisIT)
			coresIS = int(sys.coresIS)
			nodesIS = int(sys.nodesIS)
			X = deepcopy(sys.X)
			exec_time = float(sys.execution_time)
			current_analysis_mode = deepcopy(sys.current_analysis_mode)
			possible_analysis_mode = deepcopy(sys.possible_analysis_mode)

			#Add the indexith analysis to the list of IT analysis + compute an associated (n*,c*)
			sys.system_update_IT_to_IS(index)
			#Compute an execution time for the new (n*,c*)
			
			
			
			if (checking_viability(deepcopy(sys))):
				sched = scheduling(sys)
				if (sched<=exec_time):
					if (sched < exec_time):
						sys.execution_time = sched
					else:
						sys.current_analysis_mode = deepcopy(current_analysis_mode)
					sys.possible_analysis_mode.add(deepcopy(sys.current_analysis_mode))
				
				else:
					sys.AnalysisIS = deepcopy(inSituAnalysis)
					sys.AnalysisIT = deepcopy(inTransitAnalysis)
					sys.nodesIS = nodesIS
					sys.coresIS = coresIS
					sys.X = X
					sys.execution_time = exec_time
					sys.current_analysis_mode = deepcopy(current_analysis_mode)
					sys.possible_analysis_mode = deepcopy(possible_analysis_mode)
			else:
				sys.AnalysisIS = deepcopy(inSituAnalysis)
				sys.AnalysisIT = deepcopy(inTransitAnalysis)
				sys.nodesIS = nodesIS
				sys.coresIS = coresIS
				sys.X = X
				sys.execution_time = exec_time
				sys.current_analysis_mode = deepcopy(current_analysis_mode)
				sys.possible_analysis_mode = deepcopy(possible_analysis_mode)

	#check if all analysis are either set in_situ or in_transit
	load_intransit = 0
	for analysis in sys.AnalysisIT:
		for _ in range(0,analysis[2]):
			load_intransit+=analysis[0]
	assert(sys.S - sys.X == load_intransit)
	#return the system with set IS and IT analysis
	return (sys)







### Return a system with a set of analysis in situ and a set of in-transit analysis
### Consider analysis by decreasing memory peak
### Parameters: system initialized with all analysis in IT and none in IS
def maxIS_decreasing_memory_peak(system,compare_peak,compare_exec_time):
	
	sys = deepcopy(system)

	tmp = list(quicksort(system.Analysis,compare_peak))
	sys.Analysis = list(tmp[::-1])
	
	exec_time = 0
		
	for index in range (0,len(sys.Analysis)):
		for _ in range(0,sys.Analysis[index][2]):
			#Save current state
			inSituAnalysis = deepcopy(sys.AnalysisIS)
			inTransitAnalysis = deepcopy(sys.AnalysisIT)
			coresIS = int(sys.coresIS)
			nodesIS = int(sys.nodesIS)
			X = deepcopy(sys.X)
			exec_time = float(sys.execution_time)
			current_analysis_mode = deepcopy(sys.current_analysis_mode)
			possible_analysis_mode = deepcopy(sys.possible_analysis_mode)

			#Add the indexith analysis to the list of IT analysis + compute an associated (n*,c*)
			sys.system_update_IT_to_IS(index)
			#Compute an execution time for the new (n*,c*)
			
			
			
			if (checking_viability(deepcopy(sys))):
				sched = scheduling(sys)
				if (sched<=exec_time):
					if (sched < exec_time):
						sys.execution_time = sched
					else:
						sys.current_analysis_mode = deepcopy(current_analysis_mode)
					sys.possible_analysis_mode.add(deepcopy(sys.current_analysis_mode))
					
				
				else:
					sys.AnalysisIS = deepcopy(inSituAnalysis)
					sys.AnalysisIT = deepcopy(inTransitAnalysis)
					sys.nodesIS = nodesIS
					sys.coresIS = coresIS
					sys.X = X
					sys.execution_time = exec_time
					sys.current_analysis_mode = deepcopy(current_analysis_mode)
					sys.possible_analysis_mode = deepcopy(possible_analysis_mode)
			else:
				sys.AnalysisIS = deepcopy(inSituAnalysis)
				sys.AnalysisIT = deepcopy(inTransitAnalysis)
				sys.nodesIS = nodesIS
				sys.coresIS = coresIS
				sys.X = X
				sys.execution_time = exec_time
				sys.current_analysis_mode = deepcopy(current_analysis_mode)
				sys.possible_analysis_mode = deepcopy(possible_analysis_mode)

	#check if all analysis are either set in_situ or in_transit
	load_intransit = 0
	for analysis in sys.AnalysisIT:
		for _ in range(0,analysis[2]):
			load_intransit+=analysis[0]
	assert(sys.S - sys.X == load_intransit)
	#return the system with set IS and IT analysis
	return (sys)







### Return a system with a set of analysis in situ and a set of in-transit analysis
### Consider analysis by increasing execution time
### Parameters: system initializes with all analysis in IT and none in IS
def maxIS_increasing_exec_time(system,compare_peak,compare_exec_time):
		
	sys = deepcopy(system)

	sys.Analysis = list(quicksort(list(sys.Analysis),compare_exec_time))
	
	exec_time = 0

	for index in range (0,len(sys.Analysis)):
		for _ in range(0,sys.Analysis[index][2]):
			#Save current state
			inSituAnalysis = deepcopy(sys.AnalysisIS)
			inTransitAnalysis = deepcopy(sys.AnalysisIT)
			coresIS = int(sys.coresIS)
			nodesIS = int(sys.nodesIS)
			X = deepcopy(sys.X)
			exec_time = float(sys.execution_time)
			current_analysis_mode = deepcopy(sys.current_analysis_mode)
			possible_analysis_mode = deepcopy(sys.possible_analysis_mode)
			
			#Add the indexith analysis to the list of IT analysis + compute an associated (n*,c*)
			sys.system_update_IT_to_IS(index)

			#Compute an execution time for the new (n*,c*)
			

			if (checking_viability(deepcopy(sys))):
				sched = scheduling(sys)
				if (sched<=exec_time):
					if (sched < exec_time):
						sys.execution_time = sched
					else:
						sys.current_analysis_mode = deepcopy(current_analysis_mode)
					sys.possible_analysis_mode.add(deepcopy(sys.current_analysis_mode))
				
				else:
					sys.AnalysisIS = deepcopy(inSituAnalysis)
					sys.AnalysisIT = deepcopy(inTransitAnalysis)
					sys.nodesIS = nodesIS
					sys.coresIS = coresIS
					sys.X = X
					sys.execution_time = exec_time
					sys.current_analysis_mode = deepcopy(current_analysis_mode)
					sys.possible_analysis_mode = deepcopy(possible_analysis_mode)
			
			else:
				#print("pas viable pour l'analyse "+str(index)+"\n\n")
				sys.AnalysisIS = deepcopy(inSituAnalysis)
				sys.AnalysisIT = deepcopy(inTransitAnalysis)
				sys.nodesIS = nodesIS
				sys.coresIS = coresIS
				sys.X = X
				sys.execution_time = exec_time
				sys.current_analysis_mode = deepcopy(current_analysis_mode)
				sys.possible_analysis_mode = deepcopy(possible_analysis_mode)
			

	#check if all analysis are either set in_situ or in_transit
	load_intransit = 0
	for analysis in sys.AnalysisIT:
		for _ in range(0,analysis[2]):
			load_intransit+=analysis[0]
	assert(sys.S - sys.X == load_intransit)

	#return the system with set IS and IT analysis
	return (sys)







### Return a system with a set of analysis in situ and a set of in-transit analysis
### Consider analysis by decreasing execution time
### Parameters: system initializes with all analysis in IT and none in IS
def maxIS_decreasing_exec_time(system,compare_peak,compare_exec_time):
		
	sys = deepcopy(system)

	tmp = list(quicksort(list(sys.Analysis),compare_exec_time))
	sys.Analysis = list(tmp[::-1])

	exec_time = 0

	for index in range (0,len(sys.Analysis)):
		for _ in range(0,sys.Analysis[index][2]):
			#Save current state
			inSituAnalysis = deepcopy(sys.AnalysisIS)
			inTransitAnalysis = deepcopy(sys.AnalysisIT)
			coresIS = int(sys.coresIS)
			nodesIS = int(sys.nodesIS)
			X = deepcopy(sys.X)
			exec_time = float(sys.execution_time)
			current_analysis_mode = deepcopy(sys.current_analysis_mode)
			possible_analysis_mode = deepcopy(sys.possible_analysis_mode)
			
			#Add the indexith analysis to the list of IT analysis + compute an associated (n*,c*)
			sys.system_update_IT_to_IS(index)

			#Compute an execution time for the new (n*,c*)
			

			if (checking_viability(deepcopy(sys))):
				sched = scheduling(sys)
				if (sched<=exec_time):
					if (sched < exec_time):
						sys.execution_time = sched
					else:
						sys.current_analysis_mode = deepcopy(current_analysis_mode)
					sys.possible_analysis_mode.add(deepcopy(sys.current_analysis_mode))
				
				else:
					sys.AnalysisIS = deepcopy(inSituAnalysis)
					sys.AnalysisIT = deepcopy(inTransitAnalysis)
					sys.nodesIS = nodesIS
					sys.coresIS = coresIS
					sys.X = X
					sys.execution_time = exec_time
					sys.current_analysis_mode = deepcopy(current_analysis_mode)
					sys.possible_analysis_mode = deepcopy(possible_analysis_mode)
			
			else:
				sys.AnalysisIS = deepcopy(inSituAnalysis)
				sys.AnalysisIT = deepcopy(inTransitAnalysis)
				sys.nodesIS = nodesIS
				sys.coresIS = coresIS
				sys.X = X
				sys.execution_time = exec_time
				sys.current_analysis_mode = deepcopy(current_analysis_mode)
				sys.possible_analysis_mode = deepcopy(possible_analysis_mode)
			

	#check if all analysis are either set in_situ or in_transit
	load_intransit = 0
	for analysis in sys.AnalysisIT:
		for _ in range(0,analysis[2]):
			load_intransit+=analysis[0]
	assert(sys.S - sys.X == load_intransit)

	#return the system with set IS and IT analysis
	return (sys)







### Algorithm that randomly picks an in-transit analysis and tries to put it in-situ
### Iterated through all analysis
def maxIS_random(system,compare_peak,compare_exec_time):
	
	sys = deepcopy(system)

	list_Analysis = deepcopy(sys.Analysis)

	exec_time = 0
		
	liste_index = range(0,len(list_Analysis),1)
	number_analysis = 0
	for ana in list_Analysis:
		number_analysis+=ana[2]

	while number_analysis>0:
		index = int(choice(liste_index))
		list_Analysis[index][2]-=1
		assert(list_Analysis[index][2]>=0)
		if(list_Analysis[index][2]==0):
			liste_index.remove(index)
		#Save current state
		inSituAnalysis = deepcopy(sys.AnalysisIS)
		inTransitAnalysis = deepcopy(sys.AnalysisIT)
		coresIS = int(sys.coresIS)
		nodesIS = int(sys.nodesIS)
		X = deepcopy(sys.X)
		exec_time = float(sys.execution_time)
		current_analysis_mode = deepcopy(sys.current_analysis_mode)
		possible_analysis_mode = deepcopy(sys.possible_analysis_mode)

		#Add the indexith analysis to the list of IT analysis + compute an associated (n*,c*)
		sys.system_update_IT_to_IS(index)
		#Compute an execution time for the new (n*,c*)
		

		if (checking_viability(deepcopy(sys))):
			sched = scheduling(sys)
			if (sched<=exec_time):
				if (sched < exec_time):
					sys.execution_time = sched
				else:
					sys.current_analysis_mode = deepcopy(current_analysis_mode)
				sys.possible_analysis_mode.add(deepcopy(sys.current_analysis_mode))				
			else:
				sys.AnalysisIS = deepcopy(inSituAnalysis)
				sys.AnalysisIT = deepcopy(inTransitAnalysis)
				sys.nodesIS = nodesIS
				sys.coresIS = coresIS
				sys.X = X
				sys.execution_time = exec_time
				sys.current_analysis_mode = deepcopy(current_analysis_mode)
				sys.possible_analysis_mode = deepcopy(possible_analysis_mode)
			
		else:
			sys.AnalysisIS = deepcopy(inSituAnalysis)
			sys.AnalysisIT = deepcopy(inTransitAnalysis)
			sys.nodesIS = nodesIS
			sys.coresIS = coresIS
			sys.X = X
			sys.execution_time = exec_time
			sys.current_analysis_mode = deepcopy(current_analysis_mode)
			sys.possible_analysis_mode = deepcopy(possible_analysis_mode)
		
		number_analysis-=1

	#check if all analysis are either set in_situ or in_transit
	load_intransit = 0
	for a in sys.AnalysisIT:
		for _ in range(0,a[2]):
			load_intransit+=a[0]

	assert(sys.S - sys.X == load_intransit)

	#return the system with set IS and IT analysis
	return (sys)







### Returns the best analysis configuration for a given system
def test_all_subsets_IS(system,compare_peak,compare_exec_time):
	analysis_subsets_list = generate_subsets(system.Analysis)

	sys = deepcopy(system)

	exec_time = 0
		
	for list_of_analysis in analysis_subsets_list:
		if (not is_equal(list_of_analysis,sys.AnalysisIS)):
			inSituAnalysis = deepcopy(sys.AnalysisIS)
			inTransitAnalysis = deepcopy(sys.AnalysisIT)
			coresIS = int(sys.coresIS)
			nodesIS = int(sys.nodesIS)
			X = deepcopy(sys.X)
			exec_time = deepcopy(sys.execution_time)
			current_analysis_mode = deepcopy(sys.current_analysis_mode)
			possible_analysis_mode = deepcopy(sys.possible_analysis_mode)
			
			#Add the indexith analysis to the list of IT analysis + compute an associated (n*,c*)
			sys.set_IS_analysis(list_of_analysis)

			#Compute an execution time for the new (n*,c*)
			

			if (checking_viability(deepcopy(sys))):
				sched = scheduling(sys)
				if (sched<=exec_time):
					if (sched < exec_time):
						sys.execution_time = sched
					else:
						sys.current_analysis_mode = deepcopy(current_analysis_mode)
					sys.possible_analysis_mode.add(deepcopy(sys.current_analysis_mode))
				else:
					sys.AnalysisIS = deepcopy(inSituAnalysis)
					sys.AnalysisIT = deepcopy(inTransitAnalysis)
					sys.nodesIS = nodesIS
					sys.coresIS = coresIS
					sys.X = X
					sys.execution_time = exec_time
					sys.current_analysis_mode = deepcopy(current_analysis_mode)
					sys.possible_analysis_mode = deepcopy(possible_analysis_mode)
			
			else:
				sys.AnalysisIS = deepcopy(inSituAnalysis)
				sys.AnalysisIT = deepcopy(inTransitAnalysis)
				sys.nodesIS = nodesIS
				sys.coresIS = coresIS
				sys.X = X
				sys.execution_time = exec_time
				sys.current_analysis_mode = deepcopy(current_analysis_mode)
				sys.possible_analysis_mode = deepcopy(possible_analysis_mode)
			

	#check if all analysis are either set in_situ or in_transit
	load_intransit = 0
	for analysis in sys.AnalysisIT:
		for _ in range(0,analysis[2]):
			load_intransit+=analysis[0]

	assert(sys.S - sys.X == load_intransit)

	#return the system with set IS and IT analysis
	return (sys)







































# --------------------------  Scheduling function -------------------------- 


###### SCHEDULING OF A SYSTEM WITH GIVEN SIMULATION, IS AND IT ANALYSIS SETS #####
##### HYPOTHESIS: embarassingly parallel tasks #####
# Return an execution time of the simulation, IS analysis and IT analysis
def scheduling(system):
	m = 0
	for analysis in system.AnalysisIS:
		for _ in range(0,analysis[2]):
			m += analysis[1]
	assert(m<=(system.nodesIS*system.memoryPerNode)-system.memory_Simu)

	## Asynchronous mode
	if (scheduling_mode==1):
		time_Simu = (((system.alphaAmdhal+((1-system.alphaAmdhal))/((system.cores-system.coresIS)*system.nodesIS)))*system.Simulation)#system.Simulation / ((system.cores-system.coresIS)*system.nodesIS) 
		
		time_IS = 0
		if system.X:
			time_IS = ((system.alphaAmdhal+((1-system.alphaAmdhal)/(system.coresIS*system.nodesIS)))*system.X)
		if (system.AnalysisIT):
			assert(system.nodes-system.nodesIS > 0)
		time_IT = 0
		for analysis in system.AnalysisIT:
			for _ in range(0,analysis[2]):
				time_IT += communication(analysis[1],system.nodes-system.nodesIS,system.bandwidthPerNode)
				time_IT += ((system.alphaAmdhal+((1-system.alphaAmdhal)/((system.nodes-system.nodesIS)*system.cores)))*analysis[0])
		return max(time_Simu,time_IS,time_IT)#list([time_Simu,time_IS,time_IT]) 

	## Synchronous mode
	elif (scheduling_mode==2):
		time_Simu = (((system.alphaAmdhal+((1-system.alphaAmdhal))/(system.cores*system.nodesIS)))*system.Simulation)#system.Simulation / ((system.cores-system.coresIS)*system.nodesIS) 
	
		time_IS = 0
		if system.X:
			time_IS = ((system.alphaAmdhal+((1-system.alphaAmdhal)/(system.cores*system.nodesIS)))*system.X)

		if (system.AnalysisIT):
			assert(system.nodes-system.nodesIS > 0)

		time_IT = 0
		for analysis in system.AnalysisIT:
			for _ in range(0,analysis[2]):
				time_IT += communication(analysis[1],system.nodes-system.nodesIS,system.bandwidthPerNode)
				time_IT += ((system.alphaAmdhal+((1-system.alphaAmdhal)/((system.nodes-system.nodesIS)*system.cores)))*analysis[0])
		
		return max(time_Simu+time_IS,time_IT)






































# --------------------------  Main loop to generate data -------------------------- #



if (not path_to_file):
	print("Error: wront path to folder to write output files.")
	exit(-1)




print("\n\n### SYSTEM SETUP ###")
print("  Number of nodes = "+str(nodes))
print("  Number of cores = "+str(cores))
print("  Total memory = "+str(memory))
print("  Memory per node = "+str(memory_per_node))
print("  Simulation makespan on 1 core = "+str(simulation))
print("  Memory used for Simulation = "+str(memory_simu))
print("  Total number of analysis = "+str(number_analysis))
print("\n\nThe simulator performs "+str(number_tests)+" tests for each total memory load tested.\n\n")
r = ""
if (scheduling_mode==1):
	r = "asynchronous"
else:
	r = "synchronous"
print("\n\nThe scheduling mode is "+r+".")


## List of the different bandwith per node (as a factor of memory per node) we will try ##
bandwidth = []
for f in bandwidth_factors:
	bandwidth.append(memory_per_node*f)



## DEPRECITED ##
type_execution = ["IS","IT/IS","IT"]



## Features from the simulator data we extract for plotting ##
fieldnames = ['execution_time', 'memoryLoadIS', 'memoryLoadIT', 'algorithm','ISnodes','ISwork','ITwork','IScores']


## List of algorithms we will try (all of the current version by default) ##
list_of_algorithms = [maxIS_increasing_memory_peak,maxIS_decreasing_memory_peak,maxIS_increasing_exec_time,maxIS_decreasing_exec_time,maxIS_random,test_all_subsets_IS]



## Create output files ##
for algorithm in list_of_algorithms:
	for i in range(1,len(bandwidth)+1):
		file_name = path_to_file+str(algorithm.__name__)+"_"+str(i)+".csv"
		with open(file_name, 'w') as csvfile:
			writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
			writer.writeheader()
			csvfile.close()



## Main loop to generate all the data ##
cpt=1
# For all different bandwith per node value we want to test
for bandwidthPerNode in bandwidth:
	print("\nCurrent bandwidth per node = "+str(bandwidthPerNode))
	# For all the different values of memory load we want to test
	for factor in total_memory_sim_ana_vector:	
	
		dic_exec_time = {}
		for algo in list_of_algorithms:
			dic_exec_time[algo.__name__] = deepcopy([0,0,0,0,0,0,0,0])

		print("		Generating data for application memory load "+str(factor)+"...")
	
		# we perform number_tests tests for each algorithm and take the average as a final result
		# we perform number_tests tests for each algorithm to take the average
		for _ in range (number_tests):	
			# Sample an analysis set
			l = list(generate_analysis_functions_wrt_simulation(simulation, memory_simu, factor*memory_simu,number_analysis))			
			# initialize system
			sys = System(nodes,cores,memory,bandwidthPerNode,alphaAmdhal,simulation,memory_simu,list(l),[],list(l))
			
			for algorithm in list_of_algorithms:
				res  = algorithm(deepcopy(sys),compare_peak,compare_exec_time)
				if (algorithm.__name__=="maxIS_random"):
					# we will run it random_algo_runs times. We start by recording first run and add the results of other runs
					average = res.execution_time
					ISnodes = res.nodesIS
					IScores = res.coresIS
					memoryLoadIS = 0
					ISwork = 0
					for analysis in res.AnalysisIS: 
						for _ in range(0,analysis[2]):
							memoryLoadIS += deepcopy(analysis[1]) # compute memory load of in situ
							ISwork += deepcopy(analysis[0]) # compute work load of in situ
					memoryLoadIT = 0
					ITwork = 0
					for analysis in res.AnalysisIT: 
						for _ in range(0,analysis[2]):
							memoryLoadIT += deepcopy(analysis[1]) # compute memory load of in transit
							ITwork += deepcopy(analysis[0]) # compute work load of in situ


					for _ in range(0,random_algo_runs-1):
						sys = System(nodes,cores,memory,bandwidthPerNode,alphaAmdhal,simulation,memory_simu,list(l),[],list(l))
						s = algorithm(deepcopy(sys),compare_peak,compare_exec_time)
						
						average += s.execution_time
						ISnodes += s.nodesIS
						IScores += s.coresIS

						for analysis in s.AnalysisIS: 
							for _ in range(0,analysis[2]):
								memoryLoadIS += deepcopy(analysis[1]) # compute memory load of in situ
								ISwork += deepcopy(analysis[0]) # compute work load of in situ

						for analysis in s.AnalysisIT: 
							for _ in range(0,analysis[2]):
								memoryLoadIT += deepcopy(analysis[1]) # compute memory load of in transit
								ITwork += deepcopy(analysis[0]) # compute work load of in situ
					
					# Save the result for random algorithm. To do so, we average the results
					# save execution time of application
					dic_exec_time[algorithm.__name__][0] += average / random_algo_runs
					
					# save analysis memory loads
					dic_exec_time[algorithm.__name__][1] += memoryLoadIS / random_algo_runs
					dic_exec_time[algorithm.__name__][2] += memoryLoadIT / random_algo_runs
					
					# save number nodes IS and cores IS
					dic_exec_time[algorithm.__name__][4] += ISnodes / random_algo_runs
					dic_exec_time[algorithm.__name__][7] += IScores / random_algo_runs

					# save work IS/IT
					dic_exec_time[algorithm.__name__][5] += ISwork / random_algo_runs
					dic_exec_time[algorithm.__name__][6] += ITwork / random_algo_runs
			
				else:
					memoryLoadIS = 0
					ISwork = 0
					for analysis in res.AnalysisIS: 
						for _ in range(0,analysis[2]):
							memoryLoadIS += deepcopy(analysis[1]) # compute memory load of in situ
							ISwork += deepcopy(analysis[0]) # compute work load of in situ
					memoryLoadIT = 0
					ITwork = 0
					for analysis in res.AnalysisIT: 
						for _ in range(0,analysis[2]):
							memoryLoadIT += deepcopy(analysis[1]) # compute memory load of in transit
							ITwork += deepcopy(analysis[0]) # compute work load of in situ
					
					# save execution time of application
					dic_exec_time[algorithm.__name__][0] += res.execution_time

					# save analysis memory loads
					dic_exec_time[algorithm.__name__][1] += memoryLoadIS
					dic_exec_time[algorithm.__name__][2] += memoryLoadIT
					
					# save number nodes IS
					dic_exec_time[algorithm.__name__][4] += res.nodesIS
					dic_exec_time[algorithm.__name__][7] += res.coresIS

					# save work IS/IT
					dic_exec_time[algorithm.__name__][5] += ISwork
					dic_exec_time[algorithm.__name__][6] += ITwork
	

		for algorithm in list_of_algorithms:
			file_name = path_to_file+str(algorithm.__name__)+"_"+str(cpt)+".csv"
			with open(file_name, 'a') as csvfile:
				writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
				writer.writerow({'execution_time':str(dic_exec_time[algorithm.__name__][0]/float(number_tests)), 'memoryLoadIS':str(dic_exec_time[algorithm.__name__][1]/float(number_tests)), 'memoryLoadIT':str(dic_exec_time[algorithm.__name__][2]/float(number_tests)), 'algorithm':algorithm.__name__, 'ISnodes':str(dic_exec_time[algorithm.__name__][4]/float(number_tests)), 'ISwork':str(dic_exec_time[algorithm.__name__][5]/float(number_tests)), 'ITwork':str(dic_exec_time[algorithm.__name__][6]/float(number_tests)), 'IScores':str(dic_exec_time[algorithm.__name__][7]/float(number_tests))})
				csvfile.close()
		del(dic_exec_time)
	cpt+=1



print("\n#### Data generated ####\n\n Processing data for plotting...")



## Filling output files with all necessary data for plotting ##
liste_algo_str = []
for e in list_of_algorithms:
	liste_algo_str.append(e.__name__)



with open(path_to_file+"configuration.csv",'wb') as resultFile:
	wr = csv.writer(resultFile, dialect='excel')
	wr.writerow(total_memory_sim_ana_vector)
	wr.writerow(bandwidth_factors)
	wr.writerow([memory_per_node])
	wr.writerow([number_tests])	
	wr.writerow([memory_simu])
	wr.writerow([memory]) # 
	wr.writerow([simulation]) # simulation work
	wr.writerow([nodes])
	wr.writerow([cores])
resultFile.close()


for b in range(1,len(bandwidth)+1):
	features_for_plotting = fieldnames


	to_plot_exec_time = []
	to_plot_memoryLoadIS = []
	to_plot_memoryLoadIT = []
	to_plot_ISnodes = []
	to_plot_IScores=[]
	to_plot_ISwork = []
	to_plot_ITwork = []

	for algo in liste_algo_str:
		to_plot_exec_time.append([])
		to_plot_memoryLoadIS.append([])
		to_plot_memoryLoadIT.append([])
		to_plot_ISnodes.append([])
		to_plot_ISwork.append([])
		to_plot_ITwork.append([])
		to_plot_IScores.append([])

	for i in range(0,len(liste_algo_str)):
		file = open(path_to_file+liste_algo_str[i]+"_"+str(b)+".csv","r")
		labels_columns = file.readline()
		labels_columns = labels_columns.split(",")

		for line in file.readlines():
			line_split = line.split(',')
			to_plot_exec_time[i].append(line_split[0])
			to_plot_memoryLoadIS[i].append(line_split[1])
			to_plot_memoryLoadIT[i].append(line_split[2])
			to_plot_ISnodes[i].append(line_split[4])
			to_plot_ISwork[i].append(line_split[5])
			to_plot_ITwork[i].append(line_split[6])
			to_plot_IScores[i].append(line.split(",")[-1:][0][:-2])



	with open(path_to_file+"exec_time"+"_"+str(b)+".csv",'wb') as resultFile:
		wr = csv.writer(resultFile, dialect='excel')
		for i in range(0,len(to_plot_exec_time)):
			wr.writerow(to_plot_exec_time[i])
	resultFile.close()


	with open(path_to_file+"memoryLoadIS"+"_"+str(b)+".csv",'wb') as resultFile:
		wr = csv.writer(resultFile, dialect='excel')
		for i in range(0,len(to_plot_memoryLoadIS)):
			wr.writerow(to_plot_memoryLoadIS[i])
	resultFile.close()


	with open(path_to_file+"memoryLoadIT"+"_"+str(b)+".csv",'wb') as resultFile:
		wr = csv.writer(resultFile, dialect='excel')
		for i in range(0,len(to_plot_memoryLoadIT)):
			wr.writerow(to_plot_memoryLoadIT[i])
	resultFile.close()



	with open(path_to_file+"ISnodes"+"_"+str(b)+".csv",'wb') as resultFile:
		wr = csv.writer(resultFile, dialect='excel')
		for i in range(0,len(to_plot_ISnodes)):
			wr.writerow(to_plot_ISnodes[i])
	resultFile.close()



	with open(path_to_file+"ISwork"+"_"+str(b)+".csv",'wb') as resultFile:
		wr = csv.writer(resultFile, dialect='excel')
		for i in range(0,len(to_plot_ISwork)):
			wr.writerow(to_plot_ISwork[i])
	resultFile.close()

	
	with open(path_to_file+"ITwork"+"_"+str(b)+".csv",'wb') as resultFile:
		wr = csv.writer(resultFile, dialect='excel')
		for i in range(0,len(to_plot_ITwork)):
			wr.writerow(to_plot_ITwork[i])
	resultFile.close()


	with open(path_to_file+"IScores"+"_"+str(b)+".csv",'wb') as resultFile:
		wr = csv.writer(resultFile, dialect='excel')
		for i in range(0,len(to_plot_IScores)):
			wr.writerow(to_plot_IScores[i])
	resultFile.close()



print("Output files written in "+path_to_file+".\n\nSimulation process terminated.")















# ********************************** BEGINNING SIMULATOR ********************************** #