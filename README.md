# Copyright (c) 2018-2022 Univ. Bordeaux #


# in situ Simulator #

Code of an in-machine analytics simulator.
The simulator is given with R plots to generate data analysis plots presented in IJHPCA submitted paper.
You will find in the following the dependencies, how to run the simulator and how to change its setup.   





### PREREQUISITES ###

Please refer to the SageMath website for all instruction.

Once installed, you can use the following command to run sage from any directory:
	
```
	sudo ln -s /your/Path/Sage/sage-x.y/sage /usr/local/bin/sage
```

To run the plotting script, you must install R. You can install Rstudio to do so:
	
```
	sudo apt-get install rstudio
```





### RUNNING THE SIMULATOR ###

0) Clone the repository

```
	git clone https://gitlab.inria.fr/vhonore/ISAV_18_simulator
```


1) We set the path by default. Your results will be stored in folder output.

```
	path_to_file = output/"
```

If you change it, please make sure to create results folders before launching the simulator (we recommand you not to touch it).
You must have a folder PATH/Fichiers_asynchronous and PATH/Fichiers_asynchronous where PATH is your desired target.


2) Fullfill the first section of simulation.sage file with the configuration you desire (paper configuration is the basic initialization)

```
	nodes = 50 # Number of nodes
	cores = 8 # Number of cores per node
	memory = 100000 # Total memory of the system
	alphaAmdhal = 0 # In current version of the simulator, must be set to 0
	simulation = 1000 # Time to run the simulation on 1 core
	memory_simu = 75000 # Amount of memory consumed by simulation
	number_analysis = 8 # Number of analysis
	memory_per_node = memory / nodes # Memory equally shared between every node


	### Represents the different bandwidth per node we want to try
	### The bandwidth per node will be equal to i*memory_per_node for i in bandwith_factors
	bandwidth_factors = [0.1,0.5,1.0,2.0]


	### In paper setup, simulation consumme 1 amount of memeory over 1.3333333 memory available
	### This vector is used to generate set of analysis whose total memory peaks additionned to the simulation memory peak sums to given vector value
	### When factor < 1.33, possibly all analysis can be done in-situ
	### Note that 2 is an extreme case where the load of analysis memory is equal to the simulation one, which never happens in reality.
	total_memory_sim_ana_vector = [1.05,1.1,1.15,1.2,1.25,1.3,1.35,1.4,1.45,1.5,1.55,1.6,1.7,1.8,1.9,2]


	### Number of time we test a configuration. Final result will be average of the number_tests runs.
	### Useful to limite the influence of random generation of analysis
	number_tests = 130


	### Number of time we specificly run random scheduling policy algorithm. Final result will be average of the random_algo_runs runs.
	random_algo_runs = 10


	### Scheduling mode decides if you process asynchronous analytics (choose 1) or synchronous analytics (choose 2)
	global scheduling_mode ()
		# 1 = asynchronous, 2 = synchronous
		scheduling_mode = 1

```

3) Launch the simulator that will generate output files for plotting 

```
	make
```

Note that this may take a long time depending on the values of

```	
	total_memory_sim_ana_vector
	number_tests
	random_algo_runs	
```





### PLOTTING ###

There are two plotting scripts:
	- one associated to asynchronous scenario, called "plot_asynchronous.R"
	- one associated to synchronous scenario, called "plot_synchronous.R"

We recommand you to use RStudio for this proposed plotting script.

1) Set the path to the files of the simulator in plot.R script

2) Run the plot.R script in the folder of asynchronous or synchronous scenario to get the plots generated in the paper. If you use your own path, be sure to copy the two scripts in the corresponding folder


# Asynchronous scenario

You can choose the algorithm you want to plot its different associated makespan (Fig.8,9 & 10 in the paper).
To do so, just select the one you want line 5 of file "plot_asynchronous.R".





### Notes ###

Simulator only support Embarrasingly parallel tasks in current version.


